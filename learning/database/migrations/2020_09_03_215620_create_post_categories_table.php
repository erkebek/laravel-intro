<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        * CREATE TABLE post_categories (id int PRIMARY KEY AUTO_INCREMENT, title VARCHAR 191);
        */
        Schema::create('post_categories', function (Blueprint $table) {
            $table->id();
            $table->string('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*
        * DROP TABLE post_categories;
        */
        Schema::dropIfExists('post_categories');
    }
}
