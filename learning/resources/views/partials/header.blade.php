<nav class="navbar navbar-expand-lg navbar-dark bg-dark px-lg-5 p-lg-0">
    <a class="navbar-brand" href="/">Сонун посттор</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/"><i class="fa fa-home"></i> Башкы бет
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/post"><i class="fa fa-paper"></i> Посттор</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/contact"><i class="fa fa-phone"></i> Байланышуу</a>
            </li>
        </ul>
    </div>
</nav>
