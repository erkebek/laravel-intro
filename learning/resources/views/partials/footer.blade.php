<footer class="px-5 py-4 bg-dark">
    <div class="row">
        <div class="col-lg-6 col-12">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-12">
                    <h4 class="">Main</h4>
                    <ul>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <h4 class="">Main</h4>
                    <ul>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <h4 class="">Main</h4>
                    <ul>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <h4 class="">Main</h4>
                    <ul>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                        <li>
                            <a href="#">Start here</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-12 text-lg-right">
            <div class="float-lg-right">
                <div class="social-links">
                    <ul>
                        <li>
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="copyright">
                    &copy; 2020 Be Happy
                    <p>
                        <a href="">Privacy Policy</a>
                        <span>&#9679;</span>
                        <a href="">Terms of Service</a>

                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
