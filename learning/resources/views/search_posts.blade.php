@extends('partials/base')

@section('title', 'Бардык посттор')

@section('content')

<div class="container">

    <div class="posts mt-5 pb-5">
        <h3 class="pb-3 mb-4 border-bottom">
            {{$query}} боюнча издөө
        </h3>

        <div class="row">
            <div class="col-12">
                <p class="">{{$posts->count()}} пост табылды</p>
            </div>
            @foreach($posts as $post)
            <div class="col-lg-4 col-md-6 col-12 mb-3">
                <div class="card">
                    <a href="/post/{{ $post->id }}">
                        <img src="https://via.placeholder.com/720x480" class="card-img-top" alt="" />
                    </a>
                    <div class="card-body d-flex flex-row p-2">
                        <div>
                            <img src="https://via.placeholder.com/70" alt="" />
                        </div>
                        <div class="ml-2">
                            <h5><a href="/post/{{ $post->id }}">{{$post->title}}</a></h5>
                            <p class="text-muted">{{$post->description}}</p>
                        </div>
                    </div>
                    <div class="card-decoration"></div>
                    <div class="card-decoration"></div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection
