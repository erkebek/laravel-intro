@extends('partials/base')

@section('content')

<div class="container">
    <div class="posts py-5">

        <div class="blog-post">
            <h2 class="blog-post-title">{{ $post->title }}</h2>
            <h4>{{ $post->description }}</h3>
            <div class="row">
                <a href="/post/update/{{$post->id}}" class="btn btn-block btn-primary">Өзгөртуу</a>
                <a href="/post/delete/{{$post->id}}" class="btn btn-block btn-danger">Өчүрүү</a>
            </div>

            <p class="blog-post-meta">{{$post->created_at->diffForHumans()}} @if($post->author) by <a href="#">{{$post->author}}</a> @endif</p>

            {{ $post->content }}
        </div>
    </div>

</div>

@endsection
