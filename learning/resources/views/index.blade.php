@extends('partials/base')

@section('title', 'Башкы бет')

@section('content')

<div class="banner text-center">
    <h1 class="display-4 mb-4 banner-title">
        Максаттарга кандай жетүү керек?
    </h1>

    <p class="mb-5 banner-content">
        The 5 regrets paint a portrait of post-industrial man, who shrinks
        himself into a shape that fits his circumstances, then turns dutifully
        till he stops.
    </p>

    <div>
        <form action="/search" method="get">
            <input class="form-control" name="query" type="text">
            <button type="submit" class="btn btn-primary">Издөө</button>
        </form>
    </div>
    <div class="blur" id="particle-js"></div>
</div>

<div class="container">
    <div class="posts py-5">
        <div class="row">
            @foreach($posts as $post)
            <div class="col-lg-4 col-md-6 col-12 mb-3">
                <div class="card">
                    <a href="/post/{{$post->id}}">
                        <img src="https://via.placeholder.com/720x480" class="card-img-top" alt="" />
                    </a>
                    <div class="card-body d-flex flex-row p-2">
                        <div>
                            <img src="https://via.placeholder.com/70" alt="" />
                        </div>
                        <div class="ml-2">
                            <h5><a href="/post/{{$post->id}}">{{$post->title}}</a></h5>
                            <p class="text-muted">{{$post->description}}</p>
                        </div>
                    </div>
                    <div class="card-decoration"></div>
                    <div class="card-decoration"></div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div class="qutoes">
        <hr />
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="quote d-flex flex-row">
                    <div>
                        <img class="rounded-circle" src="https://via.placeholder.com/100" alt="" />
                    </div>
                    <div class="p-2">
                        <p class="m-0 text-muted">Oct 15</p>
                        <p class="m-0">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <div class="quote d-flex flex-row">
                    <div>
                        <img class="rounded-circle" src="https://via.placeholder.com/100" alt="" />
                    </div>
                    <div class="p-2">
                        <p class="m-0 text-muted">Oct 15</p>
                        <p class="m-0">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <div class="quote d-flex flex-row">
                    <div>
                        <img class="rounded-circle" src="https://via.placeholder.com/100" alt="" />
                    </div>
                    <div class="p-2">
                        <p class="m-0 text-muted">Oct 15</p>
                        <p class="m-0">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <hr />
    </div>

    <div class="featured mb-5">
        <div class="row">
            <div class="col-lg-2 col-12">
                <span>Featured on:</span>
            </div>
            <div class="col-lg-10 col-12">
                <a class="d-inline-block m-2" href="">
                    <img src="https://via.placeholder.com/150x70" alt="" />
                </a>
                <a class="d-inline-block m-2" href="">
                    <img src="https://via.placeholder.com/150x70" alt="" />
                </a>
                <a class="d-inline-block m-2" href="">
                    <img src="https://via.placeholder.com/150x70" alt="" />
                </a>
                <a class="d-inline-block m-2" href="">
                    <img src="https://via.placeholder.com/150x70" alt="" />
                </a>
                <a class="d-inline-block m-2" href="">
                    <img src="https://via.placeholder.com/150x70" alt="" />
                </a>
            </div>
        </div>
    </div>
</div>

@endsection
