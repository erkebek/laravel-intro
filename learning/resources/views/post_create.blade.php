@extends('partials/base')

@section('title', 'Create')

@section('content')
<div class="banner text-center">
    <h1 class="display-4 mb-4 banner-title">
        Post Create
    </h1>

    <p class="mb-5 banner-content">
        The 5 regrets paint a portrait of post-industrial man, who shrinks
        himself into a shape that fits his circumstances, then turns dutifully
        till he stops.
    </p>
    <div class="blur"></div>
</div>

<div class="container">
    <form class="needs-validation my-5" novalidate action="/post/save" method="post">
        @csrf
        <div class="row">
            <div class="col-12 mb-3">
                <label for="firstName">Title</label>
                <input type="text" class="form-control @error('title')is-invalid @enderror" name="title" placeholder="" value="" required="">
                @error('title')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
            <div class="col-12 mb-3">
                <label for="lastName">Description</label>
                <input type="text" class="form-control" name="description" placeholder="" value="" required="">
                <div class="invalid-feedback">
                    Valid last name is required.
                </div>
            </div>
        </div>

        <div class="mb-3">
            <label for="username">Content</label>
            <textarea type="text" class="form-control @error('content') is-invalid @enderror" rows="10" name="content" required=""></textarea>
            @error('content')
            <div class="invalid-feedback" style="width: 100%;">
                {{$message}}
            </div>
            @enderror
        </div>

        <hr class="mb-4">
        <button class="btn btn-success btn-lg btn-block" type="submit">Continue to checkout</button>
    </form>

</div>
@endsection
