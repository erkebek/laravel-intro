@extends('partials/base')

@section('title', 'Бардык посттор')

@section('content')

<div class="container">

    <div class="nav-scroller py-1 mb-2">
        <nav class="nav d-flex justify-content-between">
            <a class="p-2 text-muted" href="#">World</a>
            <a class="p-2 text-muted" href="#">U.S.</a>
            <a class="p-2 text-muted" href="#">Technology</a>
            <a class="p-2 text-muted" href="#">Design</a>
            <a class="p-2 text-muted" href="#">Culture</a>
            <a class="p-2 text-muted" href="#">Business</a>
            <a class="p-2 text-muted" href="#">Politics</a>
            <a class="p-2 text-muted" href="#">Opinion</a>
            <a class="p-2 text-muted" href="#">Science</a>
            <a class="p-2 text-muted" href="#">Health</a>
            <a class="p-2 text-muted" href="#">Style</a>
            <a class="p-2 text-muted" href="#">Travel</a>
        </nav>
    </div>

    <div class="posts pb-5">
        <h3 class="pb-3 mb-4 border-bottom">
            Technology
        </h3>

        <a href="/post/create" class="btn btn-primary">Create post</a>
        <div class="row">
            @foreach($posts as $post)
            <div class="col-lg-4 col-md-6 col-12 mb-3">
                <div class="card">
                    <a href="/post/{{ $post->id }}">
                        <img src="https://via.placeholder.com/720x480" class="card-img-top" alt="" />
                    </a>
                    <div class="card-body d-flex flex-row p-2">
                        <div>
                            <img src="https://via.placeholder.com/70" alt="" />
                        </div>
                        <div class="ml-2">
                            <h5><a href="/post/{{ $post->id }}">{{$post->title}}</a></h5>
                            <p class="text-muted">{{$post->description}}</p>
                        </div>
                    </div>
                    <div class="card-decoration"></div>
                    <div class="card-decoration"></div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection
