<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('created_at', 'DESC')->limit(3)->get();
        return view('index', [
            'posts' => $posts
        ]);
    }

    public function contact()
    {
        return view('contact');
    }

    public function posts()
    {
        return view('posts');
    }
}
