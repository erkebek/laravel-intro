<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function loginForm()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if($request->get('email') == 'erkebek.ab@gmail.com')
        {
            if($request->get('password') == '123')
            {
                return redirect('/');
            }
            return redirect()->back()->with('message', 'Password incorrect');
        }
        return redirect()->back()->with('message', 'email incorrect');
    }
}
