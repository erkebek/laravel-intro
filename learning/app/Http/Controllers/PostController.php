<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('created_at', 'DESC')->get();
        return view('posts', [
            'posts' => $posts
        ]);
    }

    public function search(Request $request)
    {
        $query = $request->get('query');
        $dictionary = [
            'mobile' => "mabile,mobil,mabil,mobili",
            'android' => "android,adroid,droid",
            'ios' => "ios,aios",
            'monitor' => "manitor,monik,mon,display",
            'laptop' => "laptop,loptop,notebook",
        ];
        foreach($dictionary as $word => $inValidOptions){
            if(in_array($query, explode(',', $inValidOptions)))
            {
                $query = $word;
            }
        }
        $posts = Post::orderBy('created_at', 'DESC')->where('title', 'LIKE', "%$query%")
            ->orWhere('description', 'LIKE', "%$query%")
            ->orWhere('content', 'LIKE', "%$query%")->get();
        return view('search_posts', [
            'posts' => $posts,
            'query' => $query
        ]);
    }

    public function create()
    {
        return view('post_create');
    }

    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('post_show', [
            'post' => $post
        ]);
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ]);
        $post = Post::create($request->all());
        return redirect('/post');
    }

    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('post_update', [
            'post' => $post
        ]);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ]);
        $post = Post::findOrFail($id);
        $post->update($request->all());
        return redirect("/post/$id");
    }

    public function delete($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        return redirect('/post');
    }
}
